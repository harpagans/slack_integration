package com.harpagans.slack;

import lombok.Lombok;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.concurrent.CyclicBarrier;

import static java.lang.Thread.sleep;

@SpringBootApplication
public class SlackApplication {

    public static void main(String[] args) {
        SpringApplication.run(SlackApplication.class, args);
    }

    @Bean
    CommandLineRunner run(@Value("${monitoring.backend.url}") String monitoringBackendUrl,
                          @Value("${slack.webhook}") String slackWebhook) {
        return args -> {
            WebClient client = WebClient.create();

            CyclicBarrier barrier = new CyclicBarrier(2);

            while (true) {
                client.get()
                        .uri(monitoringBackendUrl)
                        .accept(MediaType.TEXT_EVENT_STREAM)
                        .exchange()
                        .map(resp -> resp.bodyToFlux(StatusDto.class)
                                .groupBy(StatusDto::getName)
                                .map(env -> Flux.just(true)
                                        .concatWith(env.map(this::IsEnvUp))
                                        .buffer(2, 1)
                                        .filter(l -> l.size() == 2) // ignore last signal when conenction is lost
                                        .map(this::didEnvGoDown)
                                        .filter(stateChange ->  stateChange != StateChange.STABLE)
                                        .map(envWentDown -> new EnvironmentStateChange(envWentDown, env.key()))))
                        .map(Flux::merge)
                        .doOnError(x -> barrierAwait(barrier))
                        .subscribe(envFlux -> {
                            System.out.println("Connected!");
                            envFlux.doFinally(x -> barrierAwait(barrier))
                                    .subscribe(env -> sendSlackNotification(client, slackWebhook, env));
                        });

                barrier.await();
                Thread.sleep(3000);
                System.out.println("Reconnecting...");
            }
        };
    }

    private StateChange didEnvGoDown(List<Boolean> pair) {
        if (pair.get(0) == pair.get(1)) {
            return StateChange.STABLE;
        }

        if (pair.get(0)) {
            return StateChange.WENT_DOWN;
        }

        return StateChange.WENT_UP;
    }

    @SneakyThrows
    private int barrierAwait(CyclicBarrier barrier) {
        return barrier.await();
    }

    private boolean IsEnvUp(StatusDto status) {
        return status.getGroups().stream()
                .allMatch(group -> group.getServices().stream()
                        .allMatch(service -> service.getStatus().equals(Status.UP)));
    }

    @SneakyThrows
    static void sendSlackNotification(WebClient client, String slackWebhook, EnvironmentStateChange env) {
        SlackDto msg;
        if (env.stateChange == StateChange.WENT_DOWN) {
            msg = new SlackDto(":fire: " + env.name + " went down! :fire:");
        } else {
            msg = new SlackDto(":beer: " + env.name + " went up :D :beer:");
        }

        client.post()
                .uri(slackWebhook)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(msg), SlackDto.class)
                .exchange()
                .subscribe(d -> d.bodyToMono(Void.class));
    }

    @lombok.Value
    static class StatusDto {
        private String name;
        private List<Group> groups;
    }

    @lombok.Value
    static class Group {
        private String name;
        private List<Service> services;
    }

    @lombok.Value
    static class Service {
        private String name;
        private Status status;
        private String version;
    }

    enum Status {
        UP, DOWN, CONNECTION_REFUSED, CONNECTION_CLOSED_PREMATURELY, UNKNOWN_ERROR, TIME_OUT
    }

    enum StateChange {
        WENT_UP, WENT_DOWN, STABLE
    }

    @lombok.Value
    static class EnvironmentStateChange {
        private StateChange stateChange;
        private String name;
    }

    @lombok.Value
    static class SlackDto {
        private String text;
    }
}
